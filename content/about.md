+++
title = 'Currículo'
date = 2024-10-04T10:20:24-03:00
draft = false
+++

# Flávio Silva Ferro

**Endereço:** Vargem Grande Paulista - SP | **Email:** flavioferro@usp.br - flavioferro9@gmail.com | **Telefone:** +55 (11) 94721-7345 | **Data de nascimento:** 18 de abril de 2003

## Formação

**2018 – 2020:** ETEC Raposo Tavares  
* Técnico em química integrado ao Ensino Médio

**2021 – 2022:** Graduação em Engenharia Ambiental pela POLI-USP  
* Curso interrompido

**2023 – presente:** Graduação em Engenharia Mecatrônica pela POLI-USP  

## Línguas

* **Português:** Fluente  
* **Inglês:** Intermediário (B2)  
* **Italiano:** Iniciante (A2)  
* **Francês:** Iniciante (A1)  

## Hard Skills

* **Programação:** Python, Matlab, Julia, LaTex, Verilog  
* **Prototipagem digital:** Revit, NX 10, Fusion360, Catia  
* **Elementos Finitos:** COMSOL, Femap (NASTRAN), Abaqus CAE  
* **Edição de foto/vídeo:** Illustrator, Photoshop, Sony Vegas  
* **Outros:** XFLR5, Pacote Office  

## Experiência

**Jan 2018 – Dez 2020:** Técnico em Química, ETEC Raposo Tavares  
* Trabalho de conclusão de curso com tema de "Atividade Antimicrobiana da Semente do Maracujá (Passiflora Edulis)"

**Dez 2021 – Presente:** Keep Flying, Grupo de extensão de AeroDesign da Poli  
* Atuação nas áreas de Cargas e Aeroelasticidade, Prototipagem Digital e Integração de Projeto  
* Artigo "Análise Aeroelástica de uma Aeronave e sua Influência no Desempenho Estrutural e Aerodinâmico" apresentado no SIICUSP 2023  
* Aplicação de otimizações multi-objetivos de aeronaves  
* Atual vice-capitão da equipe e capitão de projeto  
* Participação nas competições nacionais de 2022, 2023 e 2024, e internacional de 2022 da SAE  

**Jan 2022 – Jun 2022:** Monitoria de Introdução à Computação, Poli/IME  
* Introdução de conceitos básicos de programação, com foco em Python, para alunos de engenharia  

**Jan 2023 – Dez 2023:** Iniciação Científica, Poli/NDF/RCGI  
* Pesquisa sobre "Modelagem e Simulação Numérica de Membranas para Separação de Gases em Estado Supercrítico"  
* Vinculado ao RCGI (Research Centre for Greenhouse Gas Innovation) e NDF (Núcleo de Dinâmica dos Fluidos) com bolsa FAPESP  
* Desenvolvimento de habilidades em CFD, transporte de massa e otimização de processos  
* Trabalho publicado e apresentado no COBEM 2023  

**Jan 2024 – Mar 2024:** Summer Job, Embraer  
* Estágio na área de Projeto Estrutural da linha Executiva da Embraer  
* Atuação no projeto de mecanismo para abertura de portas de emergência  
* Acompanhamento da linha de montagem de aeronaves comerciais  

**Mar 2024 – Jul 2024:** Monitoria de Fenômenos de Transporte, Poli  
* Monitor da disciplina de Fenômenos de Transporte para a turma de Engenharia Ambiental  
* Apoio na elaboração de experimentos e apresentação de conceitos sobre mecânica dos fluidos, transferência de calor e transferência de massa  

## Atividades Extracurriculares e Conquistas

* **Jun 2015:** Escrevendo um Roteiro - Oficina de criação  
* **Abr 2015 - Out 2015:** Cine-Musical 2015 | Oficina de Roteiro | Filmagem | Edição  
* **Jan 2017 - Dez 2017:** Aulas particulares de matemática e física  
* **Out 2018:** Participação na 21ª Olimpíada Brasileira de Astronomia e Astronáutica  
* **Nov 2019:** Participação na IV Olimpíada Nacional de Ciências  
* **Set 2020:** 1º lugar na Competição USP de Conhecimentos (CUCo 2020)  
* **Fev 2021:** Palestra sobre "A Crise Hídrica Atual: Contexto e Mitigação"  
* **Mar 2021:** Curso "Cities and Climate Change" oferecido pela ONU  
* **Mar 2021:** Curso "Microsoft Excel" oferecido pela Fundação Bradesco  
* **Nov 2021:** Curso "Doutrina Política: Socialismo" oferecido pelo Instituto Legislativo Brasileiro  
* **Dez 2021:** Curso "Educação Financeira" oferecido pela Fundação Bradesco  
* **Dez 2021:** Curso "Aprendizado Acelerado de Idiomas" oferecido pela Kultivi  
* **Dez 2021:** Curso "Ancient Masterpieces of World Literature" oferecido por Harvard  
* **Dez 2023:** Participação no 27º Congresso Internacional de Engenharia Mecânica (COBEM 2023)  
* **Mar 2023:** Participação no 10º Fórum SAE BRASIL AeroDesign  

## Esportes e Hobbies

* **2009 - 2012:** Futebol  
* **2012:** Aulas de teatro  
* **2014:** Natação  
* **2016:** Xadrez  
* **2019 - 2021:** Aulas de guitarra  
* **2024:** Piloto online na rede IVAO  
