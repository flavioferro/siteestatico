+++
title = "Iniciação Científica"
image = "images/ic.jpeg"
description = "Como foi fazer uma iniciação científica em um laboratório da POLI-USP."
+++

Durante minha iniciação científica, atuei no **Núcleo de Dinâmica de Fluidos (NDF)** e no **Research Centre for Greenhouse Gas Innovation (RCGI)**, onde desenvolvi um projeto intitulado *Modelagem e Simulação Numérica de Membranas para Separação de Gases em Estado Supercrítico*. Este projeto teve como objetivo explorar novas abordagens para a separação de gases, utilizando membranas, que são fundamentais em diversas aplicações industriais, como captura de carbono e purificação de gases.

### Objetivos e Metodologia

O principal objetivo da pesquisa foi analisar o desempenho de diferentes tipos de membranas sob condições de estado supercrítico, utilizando técnicas avançadas de modelagem e simulação. Para isso, utilizei ferramentas de **Dinâmica de Fluidos Computacional (CFD)**, que permitiram simular o comportamento dos gases e otimizar o design das membranas.

### Resultados e Contribuições

Durante o desenvolvimento do projeto, obtivemos resultados significativos que contribuirão para a melhoria dos processos de separação de gases. O trabalho culminou em uma apresentação no **COBEM 2023 (International Congress of Mechanical Engineering)**, onde compartilhei nossas descobertas com a comunidade acadêmica e profissional. Esta experiência não apenas aprimorou minhas habilidades técnicas, mas também me proporcionou um profundo entendimento das dinâmicas envolvidas na separação de gases e o impacto dessa tecnologia no combate às mudanças climáticas.

### Impacto e Futuro

A experiência adquirida na iniciação científica não só solidificou meu interesse em pesquisa e desenvolvimento na área de engenharia, mas também me preparou para desafios futuros na indústria e academia. Estou ansioso para aplicar o conhecimento adquirido em projetos futuros, contribuindo para inovações que promovam a sustentabilidade e a eficiência energética.
