+++
title = "Keep Flying"
image = "images/KF22.jpeg"
description = "Como se deu minha experiência na equipe de AeroDesign da POLI-USP."
+++


A **Keep Flying** é um grupo de extensão da Poli/USP dedicado ao estudo e desenvolvimento na área de aeronáutica e aeroespacial. O grupo oferece aos estudantes a oportunidade de aplicar os conhecimentos teóricos adquiridos em sala de aula em projetos práticos e inovadores. Além disso, a equipe participa de competições que desafiam as habilidades e criatividade dos membros.

## Projetos em que participei

**Análise Aeroelástica de Aeronaves**  
Participei da pesquisa e elaboração do artigo intitulado "Análise Aeroelástica de uma Aeronave e sua Influência no Desempenho Estrutural e Aerodinâmico", apresentado no SIICUSP 2023. Essa análise envolve a integração de modelos estruturais e aerodinâmicos, visando aprimorar o desempenho das aeronaves em voo.

**Competições SAE**  
Fui membro ativo da equipe durante as competições nacionais de 2022, 2023 e 2024, além da competição internacional de 2022 da SAE. Essas competições desafiam as equipes a projetar, construir e testar aeronaves, proporcionando um aprendizado prático intensivo e um ambiente competitivo que estimula a inovação.

**Otimizações Multi-Objetivos**  
Atuei na aplicação de técnicas de otimização multi-objetivos em projetos de aeronaves, buscando melhorar o desempenho e a eficiência das mesmas. Nesse trabalho, consideramos múltiplos critérios, como peso, estabilidade e consumo de energia, para alcançar soluções mais eficazes.

**Vice-Capitão da Equipe**  
Atualmente, ocupo o cargo de vice-capitão da equipe, onde contribuo para a coordenação e gestão do grupo, além de auxiliar na orientação dos novos membros. Essa posição me proporciona uma valiosa experiência em liderança e trabalho em equipe.

A experiência na **Keep Flying** não apenas ampliou meus conhecimentos técnicos na área de aeronáutica, mas também me proporcionou habilidades importantes em gestão de projetos, comunicação e colaboração. Estou entusiasmado para continuar contribuindo com a equipe e enfrentar novos desafios na aviação.


