+++
title = "Summer Job Embraer"
image = "images/embraer.jpeg"
description = "Como foi participar do summer job da Embraer."
draft = false
+++

Trabalhar na Embraer foi uma oportunidade enriquecedora que me proporcionou um profundo entendimento sobre a indústria aeronáutica e o desenvolvimento de projetos estruturais. Durante meu estágio na área de Projeto Estrutural da linha Executiva, pude aplicar conhecimentos teóricos adquiridos ao longo da minha formação em Engenharia Mecatrônica.

Minha atuação focou no projeto de mecanismos para a abertura de portas de emergência, onde aprendi a importância de integrar segurança e funcionalidade em um ambiente altamente regulado. Acompanhando a linha de montagem de aeronaves comerciais, tive a chance de observar as práticas de engenharia de ponta e os padrões de qualidade que fazem da Embraer uma referência global no setor.

A experiência foi não apenas técnica, mas também colaborativa. Trabalhar ao lado de profissionais experientes me ensinou sobre o valor do trabalho em equipe e a comunicação eficaz entre diferentes departamentos. Essa vivência consolidou meu interesse pela aviação e me motivou a buscar inovações na área, reafirmando meu compromisso com o desenvolvimento sustentável e a excelência no design de aeronaves.